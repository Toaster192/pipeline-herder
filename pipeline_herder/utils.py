"""Various utils used across the pipeline herder."""
from distutils.util import strtobool
from functools import cached_property
from functools import lru_cache
import re

from cki_lib import gitlab
from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import dateutil.parser
from rcdefinition.rc_data import parse_config_data

from . import settings

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


class CachedJob:
    """Cache information about a GitLab job to minimize API calls."""

    def __init__(self, gitlab_host, project, job_id):
        """Initialize a cached job."""
        self.gitlab_host = gitlab_host
        self.project = project
        self.job_id = job_id

    @cached_property
    def gl_instance(self):
        """Return the GitLab instance."""
        return gitlab.get_instance(f'https://{self.gitlab_host}')

    @cached_property
    def gl_project(self):
        """Return the GitLab project."""
        return self.gl_instance.projects.get(self.project)

    @cached_property
    def gl_pipeline(self):
        """Return the GitLab pipeline."""
        return self.gl_project.pipelines.get(self.gl_job.pipeline['id'])

    @cached_property
    def gl_pipeline_jobs(self):
        """Return all jobs of the GitLab pipeline."""
        return self.gl_pipeline.jobs.list(all=True, include_retried=True)

    @cached_property
    def variables(self):
        """Return the GitLab pipeline variables."""
        return {v.key: v.value for v in self.gl_pipeline.variables.list()}

    @cached_property
    def retriggered(self):
        """Return whether the pipeline was retriggered."""
        return strtobool(self.variables.get('retrigger', 'false'))

    @cached_property
    def gl_job(self):
        """Return the GitLab job."""
        return self.gl_project.jobs.get(self.job_id)

    @cached_property
    def trace(self):
        """Return the GitLab job trace."""
        return self.gl_job.trace().decode('utf8').split('\n')

    # This does not work yet for artifacts in S3!
    @lru_cache
    def artifact_file(self, name, split=True):
        """Return one artifact file or an empty list if not found."""
        try:
            artifact = self.gl_job.artifact(name).decode('utf-8')
        # pylint: disable=broad-except
        except Exception:
            return []
        if split:
            artifact = artifact.split('\n')
        return artifact

    @cached_property
    def auth_user_id(self):
        """Return the user owning the GitLab connection."""
        instance = self.gl_instance
        if not hasattr(instance, 'user'):
            instance.auth()
        return instance.user.id

    def job_name_count(self):
        """Return the number of jobs in the pipeline with the same name."""
        return len([j for j in self.gl_pipeline_jobs
                    if j.name == self.gl_job.name])

    def retry_delay(self):
        """Return the number of minutes to wait before a retry."""
        delay_index = self.job_name_count() - 1
        if delay_index >= len(settings.HERDER_RETRY_DELAYS):
            return settings.HERDER_RETRY_DELAYS[-1]
        return settings.HERDER_RETRY_DELAYS[delay_index]

    def is_retry_unsafe(self):
        # pylint: disable=too-many-return-statements
        """Check whether a job can be safely retried by the herder.

        Returns None if the job can be retried or a string with the error
        message.
        """
        # do not retry retriggered pipelines
        if self.retriggered:
            return 'retriggered job'

        # there should be no newer job with the same name
        newer_jobs = [f'J{j.id}' for j in self.gl_pipeline_jobs
                      if j.name == self.gl_job.name and j.id > self.gl_job.id]
        if newer_jobs:
            return f'job restarted externally as {", ".join(newer_jobs)}'

        # only retry up to a maximum number of times
        if self.job_name_count() > settings.HERDER_RETRY_LIMIT:
            return f'maximum of {settings.HERDER_RETRY_LIMIT} retries reached'

        # only retry if the herder is configured to do so
        if settings.HERDER_ACTION != 'retry':
            return f'HERDER_ACTION={settings.HERDER_ACTION}'

        if not misc.is_production():
            return 'IS_PRODUCTION=False'

        return None


class Matcher:  # pylint: disable=too-few-public-methods
    """Base class for matchers."""

    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, name, description, messages,
                 job_name=None, job_status=('failed',), file_name=None,
                 action='retry', tail_lines=300,
                 always_notify=False):
        """Initialize the matcher."""
        self.name = name
        self.description = description
        self.job_name = job_name
        self.job_status = job_status
        self.file_name = file_name
        self.action = action
        self.messages = messages if isinstance(messages, list) else [messages]
        self.tail_lines = tail_lines
        self.always_notify = always_notify

    @staticmethod
    def _check_lines(message, lines):
        """Check lines for a match."""
        if isinstance(message, re.Pattern):
            return message.search('\n'.join(lines))
        return any(message in line for line in lines)

    def check_lines(self, lines):
        """Check lines for a match."""
        return any(self._check_lines(m, lines) for m in self.messages)

    def check(self, job: CachedJob):
        """Check status/log for a match."""
        if job.gl_job.status not in self.job_status:
            return False
        if self.job_name and not job.gl_job.name.startswith(self.job_name):
            return False
        if self.file_name:
            all_lines = job.artifact_file(self.file_name)
        else:
            all_lines = job.trace

        return self.check_lines(all_lines[-self.tail_lines:])

    @staticmethod
    def get_affected_job(job: CachedJob):
        """Return the job that needs to be restarted."""
        return job


class RCMatcher:  # pylint: disable=too-few-public-methods
    """Check that certain keys are present on the rc file."""

    # pylint: disable=too-many-arguments
    def __init__(self, name, description, checks,
                 job_name=None, rc_path='rc', action='retry',
                 always_notify=False):
        """
        Initialize the matcher.

        name: Name to identify the matcher.
        description: Description of the matcher.
        checks: dict of {'key': re.compile(r'value')} that must be
            present on the rc file. Key is specified as {section}/{key}.
        job_name: job name if the check needs to be performed on
            an specific job.
        action: action to perform if the check fails.
        if_failure: This job should be considered a failure.
        """
        self.name = name
        self.description = description
        self.checks = checks
        self.job_name = job_name
        self.action = action
        self.rc_path = rc_path
        self.always_notify = always_notify

    @staticmethod
    def _check_all(rc_data, checks):
        """Iterate over all the checks and return True if any is missing."""
        for key, expected in checks.items():
            value = misc.get_nested_key(rc_data, key)
            if not value:
                return True
            if not expected.match(value):
                return True

        return False

    def check(self, job: CachedJob):
        """Check that rc file contains the expected key."""
        if self.job_name and not job.gl_job.name.startswith(self.job_name):
            return False

        rc_file = job.artifact_file(self.rc_path, split=False)
        if not rc_file:
            # The rc file does not exist.
            return True

        rc_data = parse_config_data(rc_file)
        return self._check_all(rc_data, self.checks)

    @staticmethod
    def get_affected_job(job: CachedJob):
        """Return the job that needs to be restarted."""
        return job


class DependingJobsMatcher:  # pylint: disable=too-few-public-methods
    """
    Checker for depending jobs.

    When a job finishes, and the job failed before, check if another one needs
    to be retriggered.

    Useful for jobs that run when=always and need to be retried after a previous
    failing job run again.
    """

    def __init__(self, job_name, depending_job_name,
                 job_status='success', action='retry',
                 always_notify=True):
        # pylint: disable=too-many-arguments
        """
        Initialize the matcher.

        job_name: Name of the first job.
        depending_job: Name of the job that needs to be retried after the first one finished.
        job_status: Necessary status of the first job.
        action: Action to perform if the check fails.
        if_failure: This job should be considered a failure.
        """
        self.job_name = job_name
        self.job_status = job_status
        self.depending_job_name = depending_job_name
        self.action = action
        self.always_notify = always_notify

        self.name = f'{self.job_name}->{self.depending_job_name} ({self.job_status})'
        self.description = self.name

    @classmethod
    def from_list(cls, rules_list):
        """Generate DependingJobsMatcher from a list of [jobs]->dependency."""
        matchers = []
        for jobs_list, dependency in rules_list:
            if not isinstance(jobs_list, list):
                jobs_list = [jobs_list]

            for job in jobs_list:
                matchers.append(
                    cls(job, dependency)
                )
        return matchers

    def check(self, job: CachedJob):
        # pylint: disable=too-many-return-statements
        """Check the job."""
        if not self.job_name == job.gl_job.name:
            return False

        if not self.job_status == job.gl_job.status:
            return False

        # there should be at least one previous failed job
        if not any(j.name == job.gl_job.name and j.id < job.gl_job.id and j.status == 'failed'
                   for j in job.gl_pipeline_jobs):
            return False

        depending_job = self.get_affected_job(job)
        if not depending_job:
            # No depending job found, so nothing that needs to be retried
            return False

        if depending_job.gl_job.status in ['canceled', 'created', 'pending']:
            # If it has not run or was cancelled, ignore it.
            return False

        if depending_job.gl_job.status == 'running':
            # If it was created before the job finished, means it was started for a different
            # job and needs to be restarted.
            depending_created_at = dateutil.parser.parse(depending_job.gl_job.created_at)
            job_finished_at = dateutil.parser.parse(job.gl_job.finished_at)
            return job_finished_at > depending_created_at

        if depending_job.gl_job.status in ['success', 'failed']:
            # It run and finished. Needs to be retried.
            return True

        # All the interesting statuses should've been handled already
        return False

    def get_affected_job(self, job: CachedJob):
        """Return the job that needs to be restarted."""
        for gl_job in job.gl_pipeline_jobs:
            if gl_job.name == self.depending_job_name:
                return CachedJob(job.gitlab_host, job.project, gl_job.id)

        return None


class NoTraceMatcher(Matcher):
    # pylint: disable=too-few-public-methods
    """Ensure the job has a trace."""

    name = 'no-trace'
    description = 'Job has no trace'
    always_notify = False

    # Retrying these jobs won't usually work.
    action = 'report'

    def __init__(self):  # pylint: disable=super-init-not-called
        """Override Matcher init parameters."""

    @staticmethod
    def check(job: CachedJob):
        """
        Check that the job's trace is not empty.

        job.trace returns a string split by new lines, so when the trace
        is empty the result will be a list with an empty string.
        """
        return job.trace == ['']


def notify_irc(job: CachedJob, notification):
    """Send a message to the IRC bot."""
    with misc.only_log_exceptions():
        status = job.gl_job.status
        name = job.gl_job.name
        job_id = job.gl_job.id
        pipeline_id = job.gl_pipeline.id
        retriggered = ' (retriggered)' if job.retriggered else ''
        url = misc.shorten_url(job.gl_job.web_url)
        message = (f'🤠 P{pipeline_id} J{job_id}{retriggered} {name} {status}:'
                   f' {notification} {url}')
        LOGGER.info('%s', message)
        if settings.IRCBOT_URL:
            SESSION.post(settings.IRCBOT_URL, json={'message': message})
